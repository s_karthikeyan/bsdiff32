#include <nan.h>
#include <stdint.h>

extern "C" {
#include "bsdiff.h"
}

#ifdef ENABLE_BSPATCH

#endif

using namespace Nan;  
using namespace v8;

NAN_METHOD(diff) {
  char *oldBin = (char *) node::Buffer::Data(info[0]->ToObject());
  uint32_t oldSize = node::Buffer::Length(info[0]->ToObject());
  
  char *newBin = (char *) node::Buffer::Data(info[1]->ToObject());
  uint32_t newSize = node::Buffer::Length(info[1]->ToObject());
  
  char *diffData = new char[oldSize + newSize];

  int32_t diffSize = bsdiff((uint8_t *)oldBin, oldSize, (uint8_t *)newBin, newSize, (uint8_t *)diffData, oldSize + newSize);

  if(diffSize <= 0) {
    info.GetReturnValue().Set(NewBuffer(0).ToLocalChecked());
    delete[] diffData;
  } else {
    info.GetReturnValue().Set(NewBuffer(diffData, diffSize).ToLocalChecked());
  }
}

#ifdef ENABLE_BSPATCH
NAN_METHOD(patch) {

}
#endif

NAN_MODULE_INIT(Init) {
  Set(target, New<String>("diff").ToLocalChecked(),
    GetFunction(New<FunctionTemplate>(diff)).ToLocalChecked());

#ifdef ENABLE_BSPATCH

#endif
}

NODE_MODULE(bsdiff32, Init)

