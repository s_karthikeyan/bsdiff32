/*
 * bsdiff modified in the following ways:
 * 1. compressed with gzip instead of bzip2, only patch generation done here
 * 2. offsets are int32_t instead of off_t
 * 3. ctrl, diff and extra blocks are interleaved instead of separate blocks
 * 4. header has 4 byte newsize
 *
 * Author: Karthikeyan Sivasambu
 *
 * Original copyright noticed retained below
 */
/*-
 * Copyright 2003-2005 Colin Percival
 * All rights reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted providing that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
//#include <unistd.h>

#define MIN(x,y) (((x)<(y)) ? (x) : (y))

static void split(int32_t *I,int32_t *V,int32_t start,int32_t len,int32_t h)
{
	int32_t i,j,k,x,tmp,jj,kk;

	if(len<16) {
		for(k=start;k<start+len;k+=j) {
			j=1;x=V[I[k]+h];
			for(i=1;k+i<start+len;i++) {
				if(V[I[k+i]+h]<x) {
					x=V[I[k+i]+h];
					j=0;
				}
				if(V[I[k+i]+h]==x) {
					tmp=I[k+j];I[k+j]=I[k+i];I[k+i]=tmp;
					j++;
				}
			}
			for(i=0;i<j;i++) V[I[k+i]]=k+j-1;
			if(j==1) I[k]=-1;
		}
		return;
	}

	x=V[I[start+len/2]+h];
	jj=0;kk=0;
	for(i=start;i<start+len;i++) {
		if(V[I[i]+h]<x) jj++;
		if(V[I[i]+h]==x) kk++;
	}
	jj+=start;kk+=jj;

	i=start;j=0;k=0;
	while(i<jj) {
		if(V[I[i]+h]<x) {
			i++;
		} else if(V[I[i]+h]==x) {
			tmp=I[i];I[i]=I[jj+j];I[jj+j]=tmp;
			j++;
		} else {
			tmp=I[i];I[i]=I[kk+k];I[kk+k]=tmp;
			k++;
		}
	}

	while(jj+j<kk) {
		if(V[I[jj+j]+h]==x) {
			j++;
		} else {
			tmp=I[jj+j];I[jj+j]=I[kk+k];I[kk+k]=tmp;
			k++;
		}
	}

	if(jj>start) split(I,V,start,jj-start,h);

	for(i=0;i<kk-jj;i++) V[I[jj+i]]=kk-1;
	if(jj==kk-1) I[jj]=-1;

	if(start+len>kk) split(I,V,kk,start+len-kk,h);
}

static void qsufsort(int32_t *I,int32_t *V,uint8_t *old,int32_t oldsize)
{
	int32_t buckets[256];
	int32_t i,h,len;

	for(i=0;i<256;i++) buckets[i]=0;
	for(i=0;i<oldsize;i++) buckets[old[i]]++;
	for(i=1;i<256;i++) buckets[i]+=buckets[i-1];
	for(i=255;i>0;i--) buckets[i]=buckets[i-1];
	buckets[0]=0;

	for(i=0;i<oldsize;i++) I[++buckets[old[i]]]=i;
	I[0]=oldsize;
	for(i=0;i<oldsize;i++) V[i]=buckets[old[i]];
	V[oldsize]=0;
	for(i=1;i<256;i++) if(buckets[i]==buckets[i-1]+1) I[buckets[i]]=-1;
	I[0]=-1;

	for(h=1;I[0]!=-(oldsize+1);h+=h) {
		len=0;
		for(i=0;i<oldsize+1;) {
			if(I[i]<0) {
				len-=I[i];
				i-=I[i];
			} else {
				if(len) I[i-len]=-len;
				len=V[I[i]]+1-i;
				split(I,V,i,len,h);
				i+=len;
				len=0;
			}
		}
		if(len) I[i-len]=-len;
	}

	for(i=0;i<oldsize+1;i++) I[V[i]]=i;
}

static int32_t matchlen(uint8_t *old,int32_t oldsize,uint8_t *new,int32_t newsize)
{
	int32_t i;

	for(i=0;(i<oldsize)&&(i<newsize);i++)
		if(old[i]!=new[i]) break;

	return i;
}

static int32_t search(int32_t *I,uint8_t *old,int32_t oldsize,
		uint8_t *new,int32_t newsize,int32_t st,int32_t en,int32_t *pos)
{
	int32_t x,y;

	if(en-st<2) {
		x=matchlen(old+I[st],oldsize-I[st],new,newsize);
		y=matchlen(old+I[en],oldsize-I[en],new,newsize);

		if(x>y) {
			*pos=I[st];
			return x;
		} else {
			*pos=I[en];
			return y;
		}
	}

	x=st+(en-st)/2;
	if(memcmp(old+I[x],new,MIN(oldsize-I[x],newsize))<0) {
		return search(I,old,oldsize,new,newsize,x,en,pos);
	} else {
		return search(I,old,oldsize,new,newsize,st,x,pos);
	}
}

static void offtout(int32_t x,uint8_t *buf)
{
	int32_t y;

	if(x<0) y=-x; else y=x;

	buf[0]=y%256;y-=buf[0];
	y=y/256;buf[1]=y%256;y-=buf[1];
	y=y/256;buf[2]=y%256;y-=buf[2];
	y=y/256;buf[3]=y%256;y-=buf[3];

	if(x<0) buf[3]|=0x80;
}

int32_t offtin(uint8_t *buf)
{
	int32_t y;

	y=buf[3]&0x7F;
	y=y*256;y+=buf[2];
	y=y*256;y+=buf[1];
	y=y*256;y+=buf[0];

	if(buf[3]&0x80) y=-y;

	return y;
}

static int bufwrite(uint8_t *buf, int32_t *offset, uint8_t *data, int32_t data_len, int32_t max_len)
{
  if(*offset + data_len > max_len)
    return -1;

  memcpy(&buf[*offset], data, data_len);
  *offset += data_len;
  return 0;
}

int32_t bsdiff(uint8_t oldbin[], int32_t oldsize, uint8_t newbin[], int32_t newsize, uint8_t diff[], int32_t maxdiffsize)
{
	int32_t *I,*V;
	int32_t scan,pos,len;
	int32_t lastscan,lastpos,lastoffset;
	int32_t oldscore,scsc;
	int32_t s,Sf,lenf,Sb,lenb;
	int32_t overlap,Ss,lens;
	int32_t i;
	int32_t dblen,eblen;
	uint8_t *db,*eb;
	uint8_t buf[4];
  int32_t diffsize = 0;

	if((I=malloc((oldsize+1)*sizeof(int32_t)))==NULL)
    return -(__LINE__);

  if((V=malloc((oldsize+1)*sizeof(int32_t)))==NULL)
    return -(__LINE__);

	qsufsort(I,V,oldbin,oldsize);

	free(V);

	if(((db=malloc(newsize+1))==NULL) ||
		((eb=malloc(newsize+1))==NULL))
    return -(__LINE__);
	dblen=0;
	eblen=0;

  /* Write the length of new file */
  if(bufwrite(diff, &diffsize, (uint8_t *)&newsize, sizeof(newsize), maxdiffsize))
    return -(__LINE__);

	/* Compute the differences, writing ctrl as we go */
	scan=0;len=0;
	lastscan=0;lastpos=0;lastoffset=0;
	while(scan<newsize) {
		oldscore=0;

		for(scsc=scan+=len;scan<newsize;scan++) {
			len=search(I,oldbin,oldsize,newbin+scan,newsize-scan,
					0,oldsize,&pos);

			for(;scsc<scan+len;scsc++)
			if((scsc+lastoffset<oldsize) &&
				(oldbin[scsc+lastoffset] == newbin[scsc]))
				oldscore++;

			if(((len==oldscore) && (len!=0)) ||
				(len>oldscore+8)) break;

			if((scan+lastoffset<oldsize) &&
				(oldbin[scan+lastoffset] == newbin[scan]))
				oldscore--;
		}

		if((len!=oldscore) || (scan==newsize)) {
			s=0;Sf=0;lenf=0;
			for(i=0;(lastscan+i<scan)&&(lastpos+i<oldsize);) {
				if(oldbin[lastpos+i]==newbin[lastscan+i]) s++;
				i++;
				if(s*2-i>Sf*2-lenf) { Sf=s; lenf=i; }
			}

			lenb=0;
			if(scan<newsize) {
				s=0;Sb=0;
				for(i=1;(scan>=lastscan+i)&&(pos>=i);i++) {
					if(oldbin[pos-i]==newbin[scan-i]) s++;
					if(s*2-i>Sb*2-lenb) { Sb=s; lenb=i; }
				}
			}

			if(lastscan+lenf>scan-lenb) {
				overlap=(lastscan+lenf)-(scan-lenb);
				s=0;Ss=0;lens=0;
				for(i=0;i<overlap;i++) {
					if(newbin[lastscan+lenf-overlap+i]==
					   oldbin[lastpos+lenf-overlap+i]) s++;
					if(newbin[scan-lenb+i]==
					   oldbin[pos-lenb+i]) s--;
					if(s>Ss) { Ss=s; lens=i+1; }
				}

				lenf+=lens-overlap;
				lenb-=lens;
			}

			for(i=0;i<lenf;i++)
				db[dblen+i]=newbin[lastscan+i]-oldbin[lastpos+i];
			for(i=0;i<(scan-lenb)-(lastscan+lenf);i++)
				eb[eblen+i]=newbin[lastscan+lenf+i];

			offtout(lenf,buf);
			//printf("ctrl 0x%08x [%02x%02x%02x%02x]", offtin(buf), buf[3], buf[2], buf[1], buf[0]);
      if(bufwrite(diff, &diffsize, buf, 4, maxdiffsize))
        return -(__LINE__);

			offtout((scan-lenb)-(lastscan+lenf),buf);
			//printf("0x%08x [%02x%02x%02x%02x]", offtin(buf), buf[3], buf[2], buf[1], buf[0]);
      if(bufwrite(diff, &diffsize, buf, 4, maxdiffsize))
        return -(__LINE__);

			offtout((pos-lenb)-(lastpos+lenf),buf);
			//printf("0x%08x [%02x%02x%02x%02x]\n", offtin(buf), buf[3], buf[2], buf[1], buf[0]);
      if(bufwrite(diff, &diffsize, buf, 4, maxdiffsize))
        return -(__LINE__);

			// Write the diff block
      if(bufwrite(diff, &diffsize, &db[dblen], lenf, maxdiffsize))
        return -(__LINE__);

			// Write the extra block
      int len_eb = (scan-lenb)-(lastscan+lenf);
      if(bufwrite(diff, &diffsize, &eb[eblen], len_eb, maxdiffsize))
        return -(__LINE__);

			dblen+=lenf;
			eblen+=(scan-lenb)-(lastscan+lenf);

      lastscan=scan-lenb;
			lastpos=pos-lenb;
			lastoffset=pos-scan;
		}
	}

	/* Free the memory we used */
	free(db);
	free(eb);
	free(I);

	return diffsize;
}

