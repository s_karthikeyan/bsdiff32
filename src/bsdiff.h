#ifndef BSDIFF_H
#define BSDIFF_H
#include <stdint.h>

int32_t bsdiff(uint8_t oldbin[], int32_t oldsize, uint8_t newbin[], int32_t newsize, uint8_t diff[], int32_t maxdiffsize);

#endif /* BSDIFF_H */

